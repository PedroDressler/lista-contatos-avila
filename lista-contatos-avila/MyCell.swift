//
//  MyCell.swift
//  lista-contatos-avila
//
//  Created by COTEMIG on 11/08/22.
//

import UIKit

class MyCell: UITableViewCell {

    @IBOutlet var labelName: UILabel!
    @IBOutlet var labelEmail: UILabel!
    @IBOutlet var labelAdress: UILabel!
    @IBOutlet var labelPhoneNumber: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
