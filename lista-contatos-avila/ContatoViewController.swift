//
//  NovoContatoViewController.swift
//  lista-contatos-avila
//
//  Created by COTEMIG on 01/09/22.
//

import UIKit

protocol ContatoViewControllerDelegate {
    func salvarNovoContato(contato: Contato)
    func editarContato()
}

class ContatoViewController: UIViewController {
    
    

    @IBOutlet var fieldNome: UITextField!
    @IBOutlet var fieldNumero: UITextField!
    @IBOutlet var fieldEmail: UITextField!
    @IBOutlet var fieldEndereco: UITextField!
    
    public var contato: Contato?
    public var delegate: ContatoViewControllerDelegate?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        fieldNome.text = contato?.nome
        fieldEmail.text = contato?.email
        fieldNumero.text = contato?.numero
        fieldEndereco.text = contato?.endereco
        
        if contato == nil {
            title = "Novo Contato"
        } else {
            title = "Editar Contato"
        }
    }

    
    @IBAction func salvarContato(_ sender: Any) {
        if contato == nil {
            let contato = Contato(
                nome: fieldNome?.text ?? "",
                email: fieldEmail?.text ?? "",
                endereco: fieldEndereco?.text ?? "",
                numero: fieldNumero?.text ?? ""
                )
            delegate?.salvarNovoContato(contato: contato)
        } else {
            contato?.nome = fieldNome?.text ?? ""
            contato?.email = fieldEmail?.text ?? ""
            contato?.endereco = fieldEndereco?.text ?? ""
            contato?.numero = fieldNumero?.text ?? ""
            
            delegate?.editarContato()
        }
        navigationController?.popViewController(animated: true)
    }
    
}

