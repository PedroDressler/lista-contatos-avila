//
//  ViewController.swift
//  lista-contatos-avila
//
//  Created by COTEMIG on 11/08/22.
//

import UIKit

class Contato: Codable {
    var nome: String
    var email: String
    var endereco: String
    var numero: String
    
    init(nome: String, email: String, endereco: String, numero: String){
        self.nome = nome
        self.email = email
        self.endereco = endereco
        self.numero = numero
    }
}

class ViewController: UIViewController, UITableViewDataSource {
    var listaContatos:[Contato] = []
    var userDefaults = UserDefaults.standard
    let listaDeContatosKey = "ListaDeContatos"
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listaContatos.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MinhaCelula", for: indexPath) as! MyCell
        let contato = listaContatos[indexPath.row]
        
        cell.labelName.text = contato.nome
        cell.labelEmail.text = contato.email
        cell.labelAdress.text = contato.endereco
        cell.labelPhoneNumber.text = contato.numero
        
        return cell
    }
    

    @IBOutlet var tabelaContatos: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tabelaContatos.dataSource = self
        tabelaContatos.delegate = self
        
        let listaData = userDefaults.value(forKey: listaDeContatosKey) as! Data
        
        do{
            let listaDeContatos = try JSONDecoder().decode([Contato].self, from: listaData)
            listaContatos = listaDeContatos
            tabelaContatos.reloadData()
        } catch {
            print("Erro em converter os dados: " + error.localizedDescription)
        }
        // Do any additional setup after loading the view.
    }
    
    func salvarContatosLocal() {
        do {
            let dataLista = try JSONEncoder().encode(listaContatos)
            userDefaults.setValue(dataLista, forKey: listaDeContatosKey)
        } catch {
            print("Erro ao salvar dados na memória local: " + error.localizedDescription)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "abrirDetalhes" {
            let detalhesViewController = segue.destination as! DetalhesViewController
            let index = sender as! Int
            let contato = listaContatos[index]
            detalhesViewController.index = index
            detalhesViewController.contato = contato
            detalhesViewController.delegate = self
            detalhesViewController.contatoDelegate = self
        } else if segue.identifier == "criarContato" {
            let contatoViewController = segue.destination as! ContatoViewController
            contatoViewController.delegate = self
        }
    }
}

extension ViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        let contato = listaContatos[indexPath.row]
        performSegue(withIdentifier: "abrirDetalhes", sender: indexPath.row)
    }
}

extension ViewController: ContatoViewControllerDelegate {
    func editarContato() {
        tabelaContatos.reloadData()
        salvarContatosLocal()
    }
    
    
    func salvarNovoContato(contato: Contato) {
        listaContatos.append(contato)
        tabelaContatos.reloadData()
        salvarContatosLocal()
    }
}

extension ViewController: DetalhesViewControllerDelegate {
    func excluirContato(index: Int) {
        listaContatos.remove(at: index)
        tabelaContatos.reloadData()
        salvarContatosLocal()
    }
}
