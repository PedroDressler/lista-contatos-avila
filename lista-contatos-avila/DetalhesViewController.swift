//
//  DetalhesViewController.swift
//  lista-contatos-avila
//
//  Created by COTEMIG on 01/09/22.
//

import UIKit

protocol DetalhesViewControllerDelegate {
    func excluirContato(index: Int)
}

class DetalhesViewController: UIViewController {

    @IBOutlet var lblNome: UILabel!
    @IBOutlet var lblNumero: UILabel!
    @IBOutlet var lblEmail: UILabel!
    @IBOutlet var lblEndereco: UILabel!
    
    @IBAction func excluirContatoTap(_ sender: Any) {
        delegate?.excluirContato(index: index!)
        navigationController?.popViewController(animated: true)
    }
    
    public var index: Int?
    public var contato: Contato?
    public var delegate: DetalhesViewControllerDelegate?
    public var contatoDelegate: ContatoViewControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        lblNome.text = contato?.nome
        lblEmail.text = contato?.email
        lblNumero.text = contato?.numero
        lblEndereco.text = contato?.endereco
        
        title = contato?.nome
        // Do any additional setup after loading the view.
    }
    
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "editarContato" {
            let contatoViewController = segue.destination as? ContatoViewController
            contatoViewController?.contato = contato
            contatoViewController?.delegate = self
        }
    }

}

extension DetalhesViewController: ContatoViewControllerDelegate {
    func salvarNovoContato(contato: Contato) {}
    
    func editarContato() {
        title = contato?.nome
        
        lblNome.text = contato?.nome
        lblNumero.text = contato?.numero
        lblEmail.text = contato?.email
        lblEndereco.text = contato?.endereco
        
        contatoDelegate?.editarContato()
    }
}
